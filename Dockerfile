FROM dev-docker-registry.tecnotree.com/common/jenkins-node-java:jdk-8

COPY spark-1.5.2.tar.gz .
#https://spark.apache.org/docs/1.5.2/building-spark.html#building-for-scala-211
#The spark tarball is built according to the above instruction

RUN tar xvf spark-1.5.2.tar.gz -C /opt
RUN rm -rf spark-*.tar.gz

RUN yum -y install nc

WORKDIR /opt/spark-1.5.2
ENV SPARK_HOME /opt/spark-1.5.2

COPY conf/log4j.properties $SPARK_HOME/conf/log4j.properties
COPY conf/spark-env.sh $SPARK_HOME/conf/spark-env.sh
COPY conf/spark-defaults.conf $SPARK_HOME/conf/spark-defaults.conf
COPY start-spark $SPARK_HOME/sbin/start-spark
COPY start.sh $SPARK_HOME/sbin/start.sh

RUN chmod +x $SPARK_HOME/sbin/start-spark
RUN chmod +x $SPARK_HOME/sbin/start.sh

ENV PATH $PATH:$SPARK_HOME/bin
ENV PATH $PATH:$SPARK_HOME/sbin
#COPY allowance-1.0-SNAPSHOT-jar-with-dependencies.jar $SPARK_HOME

EXPOSE 8080 7077 8888 8081 4040 7001 7002 7003 7004 7005 7006 7093
ENTRYPOINT ["/opt/spark-1.5.2/sbin/start.sh"]
